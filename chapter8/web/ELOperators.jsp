<html>
<body>
    <%-- ------------------------------------------------------------------------------------------------------------- --%>

	<%-- . operator, use with bean, map --%>

	<%-- . operator, simply bean property --%>
	${person.name} 
	
	<%-- . operator, nested bean property --%>
	${person.name.firstname}

	<%-- . operator, list, NOT working --%>
	${favoriteFoods.1}

	<%-- ------------------------------------------------------------------------------------------------------------- --%>

	<%-- [] operator, use with bean, map, list, array --%>

	<%-- same as . operator --%>
	${person[“name”]}

	<%-- [] operator, use bean, map, list, array --%>
	Foods are: ${favoriteFoods}

	<%-- [] operator, with int index --%>
	First food is ${favoriteFoods[0]}

	<%-- [] operator, with String number index --%>
	Second food is ${favoriteFoods["1"]}

	<%-- [] operator, with String literal number index, NOT working --%>
	<%-- ${favoriteFoods[“one”]} --%>

	<%-- [] operator, search attribute name match 'one', if there is not one return null --%>
	${favoriteFoods[one]}

	<%-- [] operator, nested expression --%>
	${favoriteFoods[numbers[0]]}
	
	<%-- ------------------------------------------------------------------------------------------------------------- --%>
</body>
</html>