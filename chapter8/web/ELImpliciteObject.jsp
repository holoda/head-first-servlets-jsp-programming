<html>
<body>
    <%-- ------------------------------------------------------------------------------------------------------------- --%>

	<%-- EL implicite objects --%>

	<%-- scope, maps --%>
	pageScope
	requestScope 
	sessionScope 
	applicationScope 
	
	<%-- request, maps --%>
	param 
	paramValues
	
	<%-- header, maps --%>
	header
	headerValues
	
	<%-- cookie, maps --%>
	cookie
	
	<%-- initParameter, maps --%>
	initParam
	
	<%-- real context!!! --%>
	pageContext
	
	<%-- ------------------------------------------------------------------------------------------------------------- --%>
	
	<%-- requestScope is not request object, NOT working, use pageContext --%>
	${requestScope.method} <%-- :( --%>
	${pageContext.request.method} <%-- :D --%>
	
</body>
</html>