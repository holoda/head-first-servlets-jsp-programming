package com.example.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.web.util.Person;

public class Servlet extends HttpServlet {

	private static final long serialVersionUID = -6209501983534514231L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("person", new Person());
		
		List<String> favoriteFoods = new ArrayList<String>();
		favoriteFoods.add("chai ice cream");
		favoriteFoods.add("fajitas");
		favoriteFoods.add("thai pizza");
		favoriteFoods.add("anything in dark chocolate");
		request.setAttribute("favoriteFoods", favoriteFoods);
		
		List<String> numbers = new ArrayList<String>();
		numbers.add("0");
		numbers.add("1");
		numbers.add("2");
		request.setAttribute("numbers", numbers);
	}
}
