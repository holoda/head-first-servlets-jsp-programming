<%@ taglib prefi="myTags" uri="simpleTags"%>
<html>
<body>
	<!-- simple tag: -->
	<myTags:simpleTag />

	<!-- simple tag with body: -->
	<myTags:simpleTagWithBody>
      This is the body
    </myTags:simpleTagWithBody>

	<!--  simple tag with expression body: -->
	<myTags:simpleTagWithExpressionBody>
      This is the body with ${message}
    </myTags:simpleTagWithExpressionBody>

	<!--  simple tag with collection expression body: -->
	<table>
		<myTags:simpleTagWithCollectionExpressionBody>
			<tr>
				<td>${movie}</td>
			</tr>
		</myTags:simpleTagWithCollectionExpressionBody>
	</table>

	<!--  simple tag with attribute: -->
	<table>
		<myTags:simpleTagWithAttribute movieList="${movieCollection}">
			<tr>
				<td>${movie.name}</td>
				<td>${movie.genre}</td>
			</tr>
		</myTags:simpleTagWithAttribute>
	</table>

	<!--  simple tag with dynamic attribute: -->
	<table>
		<myTags:simpleTagWithAttribute movieList="${movieCollection} test="hello">
			<tr>
				<td>${movie.name}</td>
				<td>${movie.genre}</td>
			</tr>
		</myTags:simpleTagWithAttribute>
	</table>

	<!--  simple tag with parent tag: -->
	<myTags:simpleTagWithBody>
		<myTags:simpleTag />
	</myTags:simpleTagWithBody>
	
</body>
</html>