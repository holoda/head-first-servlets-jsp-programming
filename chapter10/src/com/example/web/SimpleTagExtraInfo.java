package com.example.web;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.ValidationMessage;
import javax.servlet.jsp.tagext.VariableInfo;

public class SimpleTagExtraInfo extends TagExtraInfo {

	// validation request time
	@Override
	public ValidationMessage[] validate(TagData data) {
		Object o = data.getAttribute("attr1");
		if (o != null && o != TagData.REQUEST_TIME_VALUE) {
			if (((String) o).toLowerCase().equals("true") || ((String) o).toLowerCase().equals("false")) {
				return null;
			} else {
				return new ValidationMessage[] { new ValidationMessage(data.getId(), "Invalid boolean value.") };
			}
		} else
			return null;
	}

	// adding scripting attributes
	@Override
	public VariableInfo[] getVariableInfo(TagData data) {
		VariableInfo[] scriptVars = new VariableInfo[3];

		// We are telling the constructor not to create
		// a new variable since we will define it in our JSP
		// loopCounter will be available in the body and
		// remainder of the JSP
		scriptVars[0] = new VariableInfo("loopCounter", "java.lang.String", false, VariableInfo.AT_BEGIN);

		// loopCounter2 will be available after the tag
		// and for the remainder of the JSP
		scriptVars[1] = new VariableInfo("loopCounter2", "java.lang.String", true, VariableInfo.AT_END);

		// loopCounter3 will be available in the tag only
		scriptVars[1] = new VariableInfo("loopCounter3", "java.lang.String", true, VariableInfo.NESTED);

		return scriptVars;
	}
}
