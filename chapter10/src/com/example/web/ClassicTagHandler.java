package com.example.web;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class ClassicTagHandler extends TagSupport {

	private static final long serialVersionUID = 6269087665013872992L;

	private int movieCounter;
	private List<String> movies = Arrays.asList("Spiderman", "Saved!", "Amelie");

	@Override
	public int doStartTag() throws JspException {
		movieCounter = 0; // reuse instance, not created new in every case
		// in case of return EVAL_BODY_INCLUDE it is necessary because after doStartTag body is evaluated once
		pageContext.setAttribute("movie", movies.get(movieCounter)); 
		movieCounter++;

		JspWriter out = pageContext.getOut();
		try {
			out.println("in doStartTag()");
		} catch (IOException ex) {
			throw new JspException("IOException- " + ex.toString());
		}
		return SKIP_BODY; // not evaluate body fragment / default value
		// return EVAL_BODY_INCLUDE; // evaluate body fragment
	}

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			out.println("in doEndTag()");
		} catch (IOException ex) {
			throw new JspException("IOException- " + ex.toString());
		}
		return EVAL_PAGE; // evaluate whole page / default value
		// return SKIP_PAGE; // skip this page
	}

	@Override
	public int doAfterBody() throws JspException {
		if (movieCounter < movies.size()) {
			pageContext.setAttribute("movie", movies.get(movieCounter));
			movieCounter++;
			return EVAL_BODY_AGAIN;
		} else {
			return SKIP_BODY; // not evaluate body / default value
		}
	}
}
