package com.example.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpSessionWithUrlRewritingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// servlet sends both (cookies and url rewriting) solution at first time
	// from the response can evaluate which solution can be used

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		request.getSession();

		out.println("<html><body>");
		out.println("<a href=\"" + response.encodeURL("/BeerTest.do") + "\">click me</a>");
		out.println("</body></html>");
	}

	// URL rewriting works with sendRedirect()
	// URL rewriting does not work with static HTML page

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");

		response.encodeRedirectURL("/BeerTest.do");
	}

}
