package com.example.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HttpSessionWithCookieServlet extends HttpServlet {

	private static final long serialVersionUID = -8885169192645991224L;

	// get a session and check whether or not created at the moment
	// request.getSession() equals request.getSession(true)

	public void doHead(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("test session attributes<br>");

		HttpSession session = request.getSession();

		if (session.isNew()) {
			out.println("This is a new session.");
		} else {
			out.println("Welcome back!");
		}
	}

	// get a pre-existent session or null
	// asked to create a new session when the response is committed, an
	// IllegalStateException is thrown.

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("test sessions<br>");

		HttpSession session = request.getSession(false);
		if (session == null) {
			out.println("no session was available");
			out.println("making one...");
			session = request.getSession();
		} else {
			out.println("there was a session!");
		}
	}

}
