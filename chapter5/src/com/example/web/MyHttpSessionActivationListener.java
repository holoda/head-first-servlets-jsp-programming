package com.example.web;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

public class MyHttpSessionActivationListener implements HttpSessionActivationListener {

	// You have an attribute class you want objects of this type to be notified
	// when their session is passivate or activate in other JVM
	// no need to register in DD

	@Override
	public void sessionWillPassivate(HttpSessionEvent se) {
		System.out.println("Session will passivate, id: " + se.getSession().getId());
	}

	@Override
	public void sessionDidActivate(HttpSessionEvent se) {
		System.out.println("Session will activate, id: " + se.getSession().getId());
	}

}
