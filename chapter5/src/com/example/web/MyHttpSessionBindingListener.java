package com.example.web;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class MyHttpSessionBindingListener implements HttpSessionBindingListener {

	// You have an attribute class you want objects of this type to be notified
	// when they are bound to or removed from a session
	// not life-cycle listener, no need to register in DD

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		System.out.println("Object is bound to session: " + event.getSession().getId() + " as: " + event.getName());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		System.out.println("Object is unbound from session: " + event.getSession().getId() + " as: " + event.getName());
	}

}
