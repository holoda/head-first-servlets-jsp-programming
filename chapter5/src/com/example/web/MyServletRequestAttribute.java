package com.example.web;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;

public class MyServletRequestAttribute implements ServletRequestAttributeListener {

	// You want to know when a request attribute has been added, removed, or replaced
	// attribute listener, need to register in DD
	
	@Override
	public void attributeAdded(ServletRequestAttributeEvent srae) {
		System.out.println("Added attribute name: " + srae.getName() + " attribute value: " + srae.getValue());
	}

	@Override
	public void attributeRemoved(ServletRequestAttributeEvent srae) {
		System.out.println("Removed attribute name: " + srae.getName() + " attribute value: " + srae.getValue());
	}

	@Override
	public void attributeReplaced(ServletRequestAttributeEvent srae) {
		System.out.println("Replaced attribute name: " + srae.getName() + " attribute value: " + srae.getValue());
	}

}
